package Model;

import java.util.Comparator;

public class EarningComparator implements Comparator<Company>{

		@Override
		public int compare(Company o1, Company o2) {
			if(o1.getIncome() <  o2.getIncome()) return -1;
		    if(o1.getIncome() == o2.getIncome()) return 0;
		    return 1;
		}
}
