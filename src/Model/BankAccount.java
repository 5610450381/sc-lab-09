package Model;

import Interface.Measurable;

public class BankAccount implements Measurable {

	private String name;
	private double balance;

	public BankAccount(String name, double balance) {
		this.name = name;
		this.balance = balance;
	}

	public String getName() {
		return this.name;
	}

	public double getBalance() {
		return this.balance;
	}

	@Override
	public double getMeasure() {
		return this.getBalance();
	}

}
